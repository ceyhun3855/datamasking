﻿using System.Threading;
using System.Threading.Tasks;
using DataMasking.Database;

namespace DataMasking_Background.Workers
{
    internal static class DataMaskingWorker
    {
        public static async Task RunAsync(DataMaskingContext databaseContext,
            CancellationToken stoppingToken)
        {
            await NotifyUsers(databaseContext, stoppingToken);
        }

        private static async Task NotifyUsers(DataMaskingContext databaseContext,
            CancellationToken stoppingToken)
        {
            //foreach (var chargeNotification in (await databaseContext.ChargeNotifications
            //             .Where(x => x.PushNotificationStatus == PushNotificationStatus.Waiting)
            //             .Include(x => x.User)
            //             .ThenInclude(x => x.UserDevices)
            //             .ToListAsync(stoppingToken))
            //         .TakeWhile(_ => !stoppingToken.IsCancellationRequested)
            //        )
            //    await NotifyUser(databaseContext, chargeNotification, chargeNotification.User,
            //        stoppingToken);
        }

        //private static async Task NotifyUser(DataDbContext databaseContext, ChargeNotification chargeNotification,
        //    User user, CancellationToken stoppingToken)
        //{
        //    {
        //        foreach (var userDevice in user.UserDevices
        //                     .Where(ud =>
        //                         ud.IsNotificationPermission && ud.FirebaseToken != null))
        //            try
        //            {
        //                var message = new Message
        //                {
        //                    Data = new Dictionary<string, string>
        //                    {
        //                        {"type", "ChargeNotification"}
        //                    },
        //                    Notification = new Notification
        //                    {
        //                        Title = chargeNotification.Title,
        //                        Body = chargeNotification.Description
        //                    },
        //                    Token = userDevice.FirebaseToken
        //                };

        //                var messaging = FirebaseMessaging.DefaultInstance;
        //                var result = await messaging.SendAsync(message, stoppingToken);
        //                chargeNotification.PushNotificationStatus = PushNotificationStatus.Sent;
        //                chargeNotification.ModifiedAt = DateTime.Now;
        //                chargeNotification.UserDeviceId = userDevice.Id;
        //                chargeNotification.FirebaseMessageId = result;
        //                await databaseContext.SaveChangesAsync(stoppingToken);
        //                Log.Information("Push notification sent:" +
        //                                " [UserId {UserId}]" +
        //                                " [DeviceId {DeviceId}",
        //                    user.Id, userDevice.Id);
        //            }
        //            catch (FirebaseMessagingException e) when (e.MessagingErrorCode == MessagingErrorCode.Unregistered)
        //            {
        //                chargeNotification.ModifiedAt = DateTime.Now;
        //                chargeNotification.PushNotificationStatus = PushNotificationStatus.Error;
        //                userDevice.FirebaseToken = null;
        //                await databaseContext.SaveChangesAsync(stoppingToken);
        //                Log.Information("Push notification sent failed:" +
        //                                " [UserId {UserId}]" +
        //                                " [DeviceId {DeviceId}] unregistered",
        //                    user.Id, userDevice.Id);
        //            }
        //            catch (Exception e)
        //            {
        //                Log.Information("Push notification sent failed:" +
        //                                " [UserId {UserId}]" +
        //                                " [DeviceId {DeviceId}" +
        //                                " [ExceptionMessage {ExceptionMessage}]",
        //                    user.Id, userDevice.Id, e.Message);
        //            }
        //    }
        //}
    }
}