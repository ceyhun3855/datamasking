﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DataMasking.Database;
using DataMasking_Background.Workers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace DataMasking_Background.Services
{
    public class DataMaskingService : BackgroundService
    {
        private readonly IServiceProvider _services;
        private CancellationToken _stoppingToken;
        private Timer? _timer;

        public DataMaskingService(IServiceProvider services)
        {
            _services = services;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            using var scope = _services.CreateScope();

            var configuration = scope.ServiceProvider.GetService<IConfiguration>();


            _stoppingToken = stoppingToken;
            _timer = new Timer(DoWork, null, TimeSpan.FromSeconds(new Random().Next(59)),
                TimeSpan.FromMinutes(1));

            return Task.CompletedTask;
        }

        private async void DoWork(object? o)
        {
            using var scope = _services.CreateScope();
            var databaseContext =
                scope.ServiceProvider.GetService<DataMaskingContext>()
                ?? throw new Exception("DatabaseContext is null");
            //Log.Information("Running push notifications task [IsRecurring {IsRecurring}]", true);

            await DataMaskingWorker.RunAsync(databaseContext, _stoppingToken);
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            return base.StopAsync(cancellationToken);
        }

        public override void Dispose()
        {
            _timer?.Dispose();
            base.Dispose();
        }
    }
}