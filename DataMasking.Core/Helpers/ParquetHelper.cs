﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using DataMasking.Database.Entities;
using Newtonsoft.Json;
using Parquet;
using Parquet.Data;
using WebHdfs.Client;

namespace DataMasking.Core.Helpers
{
    public class ParquetHelper
    {
        public static void WriteLog(string tableName, string[] message)
        {
            WriteToParquet(tableName, message);
        }

        private static void WriteToParquet(string tableName, string[] messages)
        {
            try
            {
                var path = CreateFolderIfUnExist(tableName);
                try
                {
                    if (new FileInfo(path).Length >= 1485760)
                        RunHdfs(path);
                }
                catch (Exception e)
                {
                    //ignore
                }


                var schema = new Schema(new DataColumn(
                    new DataField<string>("name"),
                    messages).Field);
                var isAppend = File.Exists(path);
                var fieldsArray = new List<DataColumn>();
                using Stream fileStream =
                    File.Open(path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read);
                switch (tableName)
                {
                    case "Cars":
                        (schema, fieldsArray) = GenerateCarsParquet(messages);

                        break;
                    case "Profiles":
                        (schema, fieldsArray) = GenerateProfilesParuet(messages);

                        break;
                    case "Devices":
                        (schema, fieldsArray) = GenerateDevicesParquet(messages);
                        break;
                }

                using var parquetWriter = new ParquetWriter(schema, fileStream, append: isAppend);
                // create a new row group in the file
                using var groupWriter = parquetWriter.CreateRowGroup();

                foreach (var item in fieldsArray) groupWriter.WriteColumn(item);


                parquetWriter.Dispose();
                groupWriter.Dispose();
                fileStream.Flush();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static (Schema, List<DataColumn>) GenerateDevicesParquet(string[] messages)
        {
            var data = messages.Select(JsonConvert.DeserializeObject<Device>).ToList();

            var deviceNameColumn = new DataColumn(
                new DataField<string>("name"),
                data.Select(x => x.Name.ToString()).ToArray());
            var osTypeColumn = new DataColumn(
                new DataField<string>("osType"),
                data.Select(x => x.OsType.ToString()).ToArray());
            var codeColumn = new DataColumn(
                new DataField<string>("deviceGeneratedCode"),
                data.Select(x => x.DeviceGeneratedCode.ToString()).ToArray());
            var devicesTimeColumn = new DataColumn(
                new DataField<string>("createdAt"),
                data.Select(x => x.CreatedAt.ToString(CultureInfo.InvariantCulture)).ToArray());
            var fieldsArray = new List<DataColumn>
            {
                devicesTimeColumn, deviceNameColumn, osTypeColumn, codeColumn
            };
            var schema = new Schema(fieldsArray.Select(x => x.Field).ToArray());

            return (schema, fieldsArray);
        }

        private static (Schema, List<DataColumn>) GenerateProfilesParuet(string[] messages)
        {
            var data = messages.Select(JsonConvert.DeserializeObject<Profile>).ToList();

            var nameColumn = new DataColumn(
                new DataField<string>("name"),
                data.Select(x => x.Name.ToString()).ToArray());

            var surnameColumn = new DataColumn(
                new DataField<string>("surname"),
                data.Select(x => x.Surname.ToString()).ToArray());

            var emailColumn = new DataColumn(
                new DataField<string>("email"),
                data.Select(x => x.Email.ToString()).ToArray());
            var innColumn = new DataColumn(
                new DataField<string>("Inn"),
                data.Select(x => x.Inn.ToString()).ToArray());
            var profilesTimeColumn = new DataColumn(
                new DataField<string>("createdAt"),
                data.Select(x => x.CreatedAt.ToString(CultureInfo.InvariantCulture)).ToArray());

            var fieldsArray = new List<DataColumn>
            {
                profilesTimeColumn, nameColumn, surnameColumn, emailColumn, innColumn
            };
            var schema = new Schema(fieldsArray.Select(x => x.Field).ToArray());

            return (schema, fieldsArray);
        }

        private static (Schema, List<DataColumn>) GenerateCarsParquet(string[] messages)
        {
            var data = messages.Select(JsonConvert.DeserializeObject<Car>).ToList();

            var timeColumn = new DataColumn(
                new DataField<string>("createdAt"),
                data.Select(x => x.CreatedAt.ToString(CultureInfo.InvariantCulture)).ToArray());

            var carsNameColumn = new DataColumn(
                new DataField<string>("name"),
                data.Select(x => x.Name).ToArray());

            var vinColumn = new DataColumn(
                new DataField<string>("vin"),
                data.Select(x => x.Vin).ToArray());

            var fieldsArray = new List<DataColumn> {timeColumn, carsNameColumn, vinColumn};
            var schema = new Schema(fieldsArray.Select(x => x.Field).ToArray());
            return (schema, fieldsArray);
        }

        private static string CreateFolderIfUnExist(string tableName)
        {
            var dt = new DateTime(1990, 1, 1);
            var fileName = "";
            var path = Path.Combine("D:\\maskingParquetFiles", tableName);
            var exists = Directory.Exists(path);
            if (!exists)
                Directory.CreateDirectory(path);
            var fileSystemInfo = new DirectoryInfo(path);

            foreach (var fileSi in fileSystemInfo.GetFileSystemInfos())
                if (dt < Convert.ToDateTime(fileSi.LastWriteTime) &&
                    new FileInfo(fileSi.FullName).Length <= 1485760)
                {
                    dt = Convert.ToDateTime(fileSi.LastWriteTime);
                    fileName = fileSi.Name;
                }

            var result = fileName == ""
                ? Path.Combine(path, DateTime.UtcNow.ToString("").Replace("/", "")
                                         .Replace(" ", "")
                                         .Replace(":", "")
                                     + ".parquet")
                : Path.Combine(path, fileName);

            return result;
        }

        private static void RunHdfs(string path)
        {
            var webhdfs = new WebHdfsClient("http://localhost:50070", "hadoopuser");

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            var result = webhdfs.UploadFileAsync(
                path,
                "MaskingFiles",
                true
            ).Result;

            Console.WriteLine("Upload successful?: " + result);
            stopWatch.Stop();
            Console.WriteLine("Time Elapsed: " + stopWatch.Elapsed);
        }
    }
}