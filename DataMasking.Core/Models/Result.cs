﻿namespace DeShop.Core.Models
{
    public class Result
    {
        public Result(bool succeeded, string message)
        {
            Succeeded = succeeded;
            Message = message;
        }

        public bool Succeeded { get; set; }
        public string Message { get; set; }

        public void AddToMessage(string text)
        {
            Message += text;
        }
    }
}