﻿namespace DeShop.Core.Models
{
    public class LoginResultModel
    {
        public string? AccessToken { get; set; }
        public string? RefreshToken { get; set; }
        public string? UserId { get; set; }
        public bool Succeeded { get; set; }
        public string? Message { get; set; }
        public bool IsUserNew { get; set; }
    }
}