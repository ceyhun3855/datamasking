﻿using System.ComponentModel;

namespace DataMasking.Core
{
    public enum Roles
    {
        // ReSharper disable once InconsistentNaming
        [Description("USER")] user = 1,

        // ReSharper disable once InconsistentNaming

        [Description("ADMIN")] admin = 3
    }
}