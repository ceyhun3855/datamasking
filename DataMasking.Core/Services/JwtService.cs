﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using DataMasking.Core.Interfaces;
using DataMasking.Database.Entities;
using Microsoft.IdentityModel.Tokens;

namespace DataMasking.Core.Services
{
    public class JwtService : IJwtService
    {
        public ImmutableDictionary<string, string>? ExtractPayloadFromExpiredAccessToken(string accessToken)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = true,
                ValidateIssuer = true,
                ValidateIssuerSigningKey = true,
                ValidateLifetime = false,
                ValidIssuer = "hello",
                ValidAudience = "hello",
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("ghfldmsyrlfhdyrmfldw"))
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            ClaimsPrincipal principal;
            SecurityToken securityToken;
            try
            {
                principal = tokenHandler.ValidateToken(accessToken, tokenValidationParameters, out securityToken);
            }
            catch (Exception)
            {
                return null;
            }

            if (!(securityToken is JwtSecurityToken jwtSecurityToken) || !jwtSecurityToken.Header.Alg.Equals(
                    SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                return null;

            return principal.Claims.ToImmutableDictionary(claim => claim.Type, claim => claim.Value);
        }

        public string GenerateForEmail(string userId, string newEmail)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("ghfldmsyrlfhdyrmfldw"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(IJwtService.JwtClaimUserId, userId),
                new Claim(IJwtService.JwtClaimNewEmail, newEmail)
            };

            var token = new JwtSecurityToken(
                "hello",
                "hello",
                claims,
                expires: DateTime.UtcNow.AddDays(1),
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public bool IsValidToken(string token, out string userId, out string newEmail, out bool expired)
        {
            expired = true;
            userId = "";
            newEmail = "";
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("ghfldmsyrlfhdyrmfldw"));
            var validator = new JwtSecurityTokenHandler();

            // These need to match the values used to generate the token
            var validationParameters = new TokenValidationParameters
            {
                ValidIssuer = "hello",
                ValidAudience = "hello",
                IssuerSigningKey = key,
                ValidateIssuerSigningKey = true,
                ValidateAudience = true
            };

            if (!validator.CanReadToken(token)) return false;

            expired = DateTime.UtcNow > validator.ReadToken(token).ValidTo;

            try
            {
                // This line throws if invalid
                var principal = validator.ValidateToken(token, validationParameters, out var _);
                // If we got here then the token is valid
                userId = principal.FindFirstValue(IJwtService.JwtClaimUserId);
                newEmail = principal.FindFirstValue(IJwtService.JwtClaimNewEmail);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string GenerateAccessToken(User user, ImmutableDictionary<string, string> payload,
            List<string> userRoles)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("ghfldmsyrlfhdyrmfldw"));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var claims = payload.Select(kv => new Claim(kv.Key, kv.Value)).ToList();
            foreach (var role in userRoles) claims.Add(new Claim(ClaimTypes.Role, role));
            var token = new JwtSecurityToken(
                "hello",
                "hello",
                claims,
                expires: DateTime.UtcNow.AddMinutes(10000),
                signingCredentials: credentials
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}