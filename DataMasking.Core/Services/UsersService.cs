using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using DataMasking.Core.Interfaces;
using DataMasking.Database;
using DataMasking.Database.Entities;
using DeShop.Core.Models;
using Microsoft.AspNetCore.Identity;

namespace DataMasking.Core.Services
{
    public class UsersService : IUsersService
    {
        private readonly DataMaskingContext _databaseContext;
        private readonly IJwtService _jwtService;
        private readonly SignInManager<User> _signInManager;

        private readonly UserManager<User> _userManager;

        public UsersService(IJwtService jwtService, UserManager<User> userManager, SignInManager<User> signInManager,
            DataMaskingContext databaseContext)
        {
            _jwtService = jwtService;
            _userManager = userManager;
            _signInManager = signInManager;
            _databaseContext = databaseContext;
        }


        public async Task<LoginResultModel> LogIn(string email, string password)
        {
            const string invalidLoginOrPasswordMessage = "Неверный логин или пароль.";
            var user = await _userManager.FindByEmailAsync(email);

            if (user == null)
                return new LoginResultModel
                {
                    Succeeded = false,
                    Message = invalidLoginOrPasswordMessage
                };
            var signInResult = await _signInManager.CheckPasswordSignInAsync(user, password, true);
            if (!signInResult.Succeeded)
                return new LoginResultModel
                {
                    Succeeded = false,
                    Message = invalidLoginOrPasswordMessage
                };
            return await GenerateSuccessfulLoginResultModel(user);
        }

        public async Task<Result> SignUp(string email, string password)
        {
            var existedUser = await _userManager.FindByEmailAsync(email);
            if (existedUser != null)
                return new Result(false, "Пользователь с таким почтовым адресом уже зарегистрирован");

            var user = new User
            {
                Email = email,
                UserName = email
            };
            var result = await _userManager.CreateAsync(user, password);

            if (!result.Succeeded)
            {
                var errors = result.Errors.Aggregate("",
                    (current, e) => current + e.Description + '\n');
                return new Result(false, errors);
            }

            await _userManager.AddToRoleAsync(user, Roles.user.ToString());
            await _databaseContext.SaveChangesAsync();
            return new Result(true, "Успешно зарегистрирован");
        }

        public async Task<bool> LogInVerify(string userId)
        {
            var user = await _databaseContext.Users.FindAsync(userId);

            if (user == null) throw new InvalidOperationException();
            return true;
        }

        public async Task<Result> LogOut(string userId, string sessionId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var refreshTokenName = IJwtService.RefreshTokenTokenPrefix + sessionId;
            var removeRefreshTokenResult = await _userManager.RemoveAuthenticationTokenAsync(user,
                IJwtService.RefreshTokenTokenProviderName, refreshTokenName);
            var result = new Result(removeRefreshTokenResult.Succeeded,
                removeRefreshTokenResult.Succeeded ? "Успешный выход" : "Ошибка");
            return result;
        }

        private async Task<(string, string)> GenerateAccessRefreshTokenAsync(User user, string sessionId,
            ImmutableDictionary<string, string> accessTokenPayload)
        {
            var userRoles = await _userManager.GetRolesAsync(user);
            var accessToken = _jwtService.GenerateAccessToken(user, accessTokenPayload, userRoles.ToList());
            var refreshTokenName = IJwtService.RefreshTokenTokenPrefix + sessionId;
            await _userManager.RemoveAuthenticationTokenAsync(user, IJwtService.RefreshTokenTokenProviderName,
                refreshTokenName);
            var refreshToken =
                await _userManager.GenerateUserTokenAsync(user, IJwtService.RefreshTokenTokenProviderName,
                    refreshTokenName);
            await _userManager.SetAuthenticationTokenAsync(user, IJwtService.RefreshTokenTokenProviderName,
                refreshTokenName,
                refreshToken);
            return (accessToken, refreshToken);
        }

        private async Task<LoginResultModel> GenerateSuccessfulLoginResultModel(User user)
        {
            var sessionId = Guid.NewGuid().ToString("N");
            var accessTokenPayload = new Dictionary<string, string>
            {
                {IJwtService.JwtClaimUserId, user.Id},
                {IJwtService.JwtClaimSessionId, sessionId}
            }.ToImmutableDictionary();
            var (accessToken, refreshToken) =
                await GenerateAccessRefreshTokenAsync(user, sessionId, accessTokenPayload);

            return new LoginResultModel
            {
                Succeeded = true,
                AccessToken = accessToken,
                RefreshToken = refreshToken,
                UserId = user.Id
            };
        }
    }
}