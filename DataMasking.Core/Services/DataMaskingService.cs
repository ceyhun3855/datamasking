﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using DataMasking.Core.Helpers;
using DataMasking.Core.Interfaces;
using DataMasking.Database;
using DataMasking.Database.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;

namespace DataMasking.Core.Services
{
    public class DataMaskingService : IDataMaskingService
    {
        private readonly DataMaskingContext _dataMaskingContext;
        private readonly List<MaskingFieldInformation> _maskingFieldInfos;

        private List<string> _profileMessages = new();
        private List<string> _carMessages = new();
        private List<string> _deviceMessages = new();

        public DataMaskingService(DataMaskingContext dataMaskingContext)
        {
            _dataMaskingContext = dataMaskingContext;
            _maskingFieldInfos = _dataMaskingContext.MaskingFieldInfos.ToList();
        }

        public void MaskData(byte[] jsonObject)
        {
            var message = Encoding.UTF8.GetString(jsonObject);
            dynamic data = JObject.Parse(message);
            string tableName = data.TableName;
            switch (tableName)
            {
                case "Cars":
                    _carMessages.Add(message);
                    if (_carMessages.Count >= 1000)
                    {
                        SendToParquet(tableName, _carMessages);
                        _dataMaskingContext.SaveChanges();
                        _carMessages = new List<string>();
                    }

                    break;
                case "Profiles":
                    _profileMessages.Add(message);
                    if (_profileMessages.Count >= 1000)
                    {
                        SendToParquet(tableName, _profileMessages);
                        _dataMaskingContext.SaveChanges();
                        _profileMessages = new List<string>();
                    }

                    break;
                case "Devices":
                    _deviceMessages.Add(message);
                    if (_deviceMessages.Count >= 1000)
                    {
                        SendToParquet(tableName, _deviceMessages);
                        _dataMaskingContext.SaveChanges();
                        _deviceMessages = new List<string>();
                    }

                    break;
            }


            try
            {
                var maskingFieldInfos = _maskingFieldInfos
                    .Where(x => x.TableName == tableName).ToList();
                switch (tableName)
                {
                    case "Profiles":
                        ParseProfiles(message,
                            maskingFieldInfos.Where(x => x.TableName == "Profiles")
                                .Select(x => x.FieldName).ToList());
                        break;
                    case "Devices":
                        ParseDevices(message,
                            maskingFieldInfos.Where(x => x.TableName == "Devices")
                                .Select(x => x.FieldName).ToList());
                        break;
                    case "Cars":
                        ParseCars(message,
                            maskingFieldInfos.Where(x => x.TableName == "Cars")
                                .Select(x => x.FieldName).ToList());
                        break;
                }

                Log.Information("Write To DB");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void SendToParquet(string tableName, List<string> messages)
        {
            ParquetHelper.WriteLog(tableName, messages.ToArray());
        }

        private void ParseProfiles(string message, List<string> fieldNames)
        {
            var receivedMessage = JsonConvert.DeserializeObject<Profile>(message);
            dynamic jsonObj = JsonConvert.DeserializeObject(message);
            foreach (var fieldName in fieldNames) jsonObj[fieldName] = "xxxMaskedxxx";
            string output = JsonConvert.SerializeObject(jsonObj, Formatting.Indented);
            var masked = JsonConvert.DeserializeObject<ProfileMasked>(output);
            if (receivedMessage != null) _dataMaskingContext.Profiles.Add(receivedMessage);
            if (masked != null) _dataMaskingContext.UsersMasked.Add(masked);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("[{0}] Receive Users {1}", DateTime.Now, message);
            Console.ResetColor();
        }


        private void ParseDevices(string message, List<string> fieldNames)
        {
            var receivedMessage = JsonConvert.DeserializeObject<Device>(message);
            dynamic jsonObj = JsonConvert.DeserializeObject(message);
            foreach (var fieldName in fieldNames) jsonObj[fieldName] = Mask((string) jsonObj[fieldName]);
            string output = JsonConvert.SerializeObject(jsonObj, Formatting.Indented);
            var masked = JsonConvert.DeserializeObject<DeviceMasked>(output);
            if (receivedMessage != null) _dataMaskingContext.Devices.Add(receivedMessage);
            if (masked != null) _dataMaskingContext.DevicesMasked.Add(masked);
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("[{0}] Receive Devices {1}", DateTime.Now, message);
            Console.ResetColor();
        }

        private void ParseCars(string message, List<string> fieldNames)
        {
            var receivedMessage = JsonConvert.DeserializeObject<Car>(message);
            dynamic jsonObj = JsonConvert.DeserializeObject(message);
            foreach (var fieldName in fieldNames) jsonObj[fieldName] = "xxxMaskedxxx";
            string output = JsonConvert.SerializeObject(jsonObj, Formatting.Indented);
            var masked = JsonConvert.DeserializeObject<CarMasked>(output);
            if (receivedMessage != null) _dataMaskingContext.Cars.Add(receivedMessage);
            if (masked != null) _dataMaskingContext.CarsMasked.Add(masked);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("[{0}] Receive Cars {1}", DateTime.Now, message);
            Console.ResetColor();
        }

        public static string Mask(string value)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (var i = 0; i < stringChars.Length; i++) stringChars[i] = chars[random.Next(chars.Length)];

            return new string(stringChars);
        }
    }
}