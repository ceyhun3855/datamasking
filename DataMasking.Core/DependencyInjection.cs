﻿using DataMasking.Core.Interfaces;
using DataMasking.Core.Services;
using Microsoft.Extensions.DependencyInjection;

namespace DataMasking.Core
{
    public static class DependencyInjection
    {
        public static void AddBusinessLogicLayerDI(this IServiceCollection services)
        {
            //add services
            services.AddScoped<IDataMaskingService, DataMaskingService>();
            services.AddScoped<IJwtService, JwtService>();
            services.AddScoped<IUsersService, UsersService>();
        }
    }
}