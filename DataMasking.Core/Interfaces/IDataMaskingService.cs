﻿namespace DataMasking.Core.Interfaces
{
    public interface IDataMaskingService
    {
        void MaskData(byte[] jsonObject);
    }
}