﻿using System.Threading.Tasks;
using DeShop.Core.Models;

namespace DataMasking.Core.Interfaces
{
    public interface IUsersService
    {
        Task<LoginResultModel> LogIn(string email, string password);
        Task<Result> SignUp(string email, string password);
    }
}