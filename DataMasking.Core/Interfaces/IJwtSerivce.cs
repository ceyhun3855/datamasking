﻿using System.Collections.Generic;
using System.Collections.Immutable;
using DataMasking.Database.Entities;

namespace DataMasking.Core.Interfaces
{
    public interface IJwtService
    {
        const string JwtClaimUserId = "user_id";
        const string JwtClaimSessionId = "session_id";
        const string JwtClaimNewEmail = "new_email";
        const string RefreshTokenTokenProviderName = "RefreshTokenTokenProvider";
        const string RefreshTokenTokenPrefix = "RefreshToken-";
        string GenerateAccessToken(User user, ImmutableDictionary<string, string> payload, List<string> userRoles);

        ImmutableDictionary<string, string>? ExtractPayloadFromExpiredAccessToken(string accessToken);

        string GenerateForEmail(string userId, string newEmail);
        bool IsValidToken(string token, out string userId, out string newEmail, out bool expired);
    }
}