﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Threading.Tasks;
using DataMasking.Core.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Serilog;

namespace DataMasking.RabbitMq
{
    public class App
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;

        private IConfiguration _configuration;

        public App(IServiceScopeFactory serviceScopeFactory,
            IConfiguration configuration)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _configuration = configuration;
        }

        public async Task RunAsync()
        {
            try
            {
                Log.Information("Starting Web Host");
                await Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.Information("Finished Web Host");
                Log.CloseAndFlush();
            }
        }

        public Task Run()
        {
            using var scope = _serviceScopeFactory.CreateScope();
            var maskingService = scope.ServiceProvider.GetRequiredService<IDataMaskingService>();
            var factory = new ConnectionFactory {HostName = "localhost", Password = "52163", UserName = "admin"};

            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare("DataMasking", false, false, false, null);
            var consumer = new EventingBasicConsumer(channel);
            byte[] body;
            consumer.Received += (_, ea) =>
            {
                body = ea.Body.ToArray();
                maskingService.MaskData(body);
            };

            channel.BasicConsume("DataMasking", true, consumer);

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
            return Task.CompletedTask;
        }
    }
}