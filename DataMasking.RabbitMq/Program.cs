﻿using System;
using System.IO;
using System.Threading.Tasks;
using DataMasking.Core;
using DataMasking.Database;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DataMasking.RabbitMq
{
    public class Program
    {
        private static string ForceEnviroment;

        private static async Task Main(string[] args)
        {
            if (args != null && args.Length != 0)
                if (args[0] == "useStaging")
                    ForceEnviroment = "Staging";


            var services = ConfigureServices();

            var serviceProvider = services.BuildServiceProvider();
            await serviceProvider.GetService<App>()?.RunAsync()!;
        }

        private static IServiceCollection ConfigureServices()
        {
            IServiceCollection services = new ServiceCollection();

            var config = LoadConfiguration();
            services.AddSingleton(config);
            services.AddTransient<DataMaskingContext>();

            services.AddBusinessLogicLayerDI();
            services.AddScoped<App>();

            return services;
        }

        public static IConfiguration LoadConfiguration()
        {
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (ForceEnviroment != null) environment = ForceEnviroment;
            Console.WriteLine("environment=" + environment);

            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddEnvironmentVariables()
                .Build();
        }
    }
}