﻿using System;
using System.Net.Mail;
using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace DataMasking.TestDataRabbitMq
{
    internal class Program
    {
        private static string RandName()
        {
            string[] maleNames =
                {"aaron", "abdul", "abe", "abel", "abraham", "adam", "adan", "adolfo", "adolph", "adrian"};
            string[] femaleNames = {"abby", "abigail", "adele", "adrian"};
            var result = "";
            var rand = new Random(DateTime.Now.Second);
            result = rand.Next(1, 2) == 1
                ? maleNames[rand.Next(0, maleNames.Length - 1)]
                : femaleNames[rand.Next(0, femaleNames.Length - 1)];

            return result;
        }

        private static void Main(string[] args)
        {
            var factory = new ConnectionFactory {HostName = "localhost", Password = "52163", UserName = "admin"};
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare("hello",
                false,
                false,
                false,
                null);


            Console.WriteLine(" Press [enter] to exit.");

            while (true) SendMessage(channel);
        }

        private static void SendMessage(IModel channel)
        {
            var user = new
            {
                TableName = "Profiles",
                Email = "mail@mail.ru",
                Inn = RandName(),
                Name = RandName(),
                Surname = RandName()
            };

            var car = new
            {
                TableName = "Cars",
                Name = RandName(),
                Vin = Guid.NewGuid()
            };

            var device = new
            {
                TableName = "Devices",
                Name = RandName(),
                OsType = new Random().Next(1, 2) == 1 ? "Android" : "Ios",
                DeviceGeneratedCode = Guid.NewGuid(),
                CreatedAd = DateTime.Now
            };
            var userMessage = Encoding.Default.GetBytes(JsonConvert.SerializeObject(user));
            var carMessage = Encoding.Default.GetBytes(JsonConvert.SerializeObject(car));
            var deviceMessage = Encoding.Default.GetBytes(JsonConvert.SerializeObject(device));
            channel.BasicPublish("",
                "DataMasking",
                null,
                userMessage);
            channel.BasicPublish("",
                "DataMasking",
                null,
                carMessage);
            channel.BasicPublish("",
                "DataMasking",
                null,
                deviceMessage);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(" [{0}] Sent Users {1}", DateTime.Now, Encoding.UTF8.GetString(userMessage));
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(" [{0}] Sent Cars {1}", DateTime.Now, Encoding.UTF8.GetString(carMessage));
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(" [{0}] Sent Devices {1}", DateTime.Now, Encoding.UTF8.GetString(deviceMessage));
            Console.ResetColor();
        }
    }
}