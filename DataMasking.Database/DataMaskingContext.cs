﻿using DataMasking.Database.Entities;
using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DataMasking.Database
{
    public class DataMaskingContext : IdentityDbContext<User>, IDataProtectionKeyContext
    {
        public DbSet<MaskingFieldInformation> MaskingFieldInfos { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ProfileMasked> ProfileMasked { get; set; }

        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<CarMasked> CarsMasked { get; set; }

        public DbSet<DeviceMasked> DevicesMasked { get; set; }
        public DbSet<ProfileMasked> UsersMasked { get; set; }

        public DbSet<Device> Devices { get; set; }
        public DbSet<DataProtectionKey> DataProtectionKeys { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(
                "Host=localhost;Port=5432;Database=DataMasking;Username=postgres;Password=postgres;");
        }
    }
}