﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataMasking.Database.Entities
{
    [Table("Cars", Schema = "Masked")]
    public class CarMasked
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Vin { get; set; }
    }
}