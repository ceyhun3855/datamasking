﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataMasking.Database.Entities
{
    [Table("Profiles", Schema = "Masked")]
    public class ProfileMasked
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Inn { get; set; }
    }
}