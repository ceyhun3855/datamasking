﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataMasking.Database.Entities
{
    [Table("Devices", Schema = "Original")]
    public class Device : Entity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string OsType { get; set; }
        public string DeviceGeneratedCode { get; set; }
    }
}