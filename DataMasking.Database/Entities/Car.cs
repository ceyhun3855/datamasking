﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataMasking.Database.Entities
{
    [Table("Cars", Schema = "Original")]
    public class Car : Entity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Vin { get; set; }
    }
}