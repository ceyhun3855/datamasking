﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataMasking.Database.Entities
{
    [Table("MaskingFieldInfos", Schema = "public")]
    public class MaskingFieldInformation
    {
        public long Id { get; set; }
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public int MaskingType { get; set; }
    }
}