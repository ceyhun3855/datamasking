﻿using System;
using Microsoft.AspNetCore.Identity;

namespace DataMasking.Database.Entities
{
    public class User : IdentityUser
    {
        public DateTime CreatedAt { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }

        public string Surname { get; set; }
    }
}