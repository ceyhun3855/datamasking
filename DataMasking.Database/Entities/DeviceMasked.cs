﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMasking.Database.Entities
{
    [Table("Devices", Schema = "Masked")]
    public class DeviceMasked
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string OsType { get; set; }
        public string DeviceGeneratedCode { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}