﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataMasking.Database.Migrations
{
    public partial class MasgingFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "Masked",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "MaskingType",
                schema: "Masked",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "Masked",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "MaskingType",
                schema: "Masked",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "Masked",
                table: "Cars");

            migrationBuilder.DropColumn(
                name: "MaskingType",
                schema: "Masked",
                table: "Cars");

            migrationBuilder.AddColumn<int>(
                name: "MaskingType",
                schema: "public",
                table: "MaskingFieldInfos",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaskingType",
                schema: "public",
                table: "MaskingFieldInfos");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "Masked",
                table: "Profiles",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "MaskingType",
                schema: "Masked",
                table: "Profiles",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "Masked",
                table: "Devices",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "MaskingType",
                schema: "Masked",
                table: "Devices",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "Masked",
                table: "Cars",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "MaskingType",
                schema: "Masked",
                table: "Cars",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }
    }
}
