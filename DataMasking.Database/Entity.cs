﻿using System;

namespace DataMasking.Database
{
    public class Entity
    {
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}