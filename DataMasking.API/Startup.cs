using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using DataMasking.API.CustomTokenProviders;
using DataMasking.Core;
using DataMasking.Core.Interfaces;
using DataMasking.Database;
using DataMasking.Database.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using NSwag;
using NSwag.Generation.Processors.Security;
using OpenApiInfo = Microsoft.OpenApi.Models.OpenApiInfo;
using OpenApiSecurityScheme = NSwag.OpenApiSecurityScheme;

namespace DataMasking.API
{
    public class Startup
    {
        private const string EmailConfirmationTokenProviderName = "ConfirmEmail";
        private const string ResetPasswordTokenProviderName = "ResetPassword";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        private void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "YourApiName",
                    Description = "Your Api Description."
                });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath); // ��� ����� ������ ����� � ���������� �������
            });
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("Access-Control-Allow-Origin", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            services.AddTransient<DataMaskingContext>();


            services.AddLogging(opt => { opt.AddConsole(c => { c.TimestampFormat = "[yyyy-MM-ddTHH:mm:ssK] "; }); });

            services.AddHttpContextAccessor();


            services.AddControllers(options => { }
            ).AddJsonOptions(opt => { opt.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()); });
            services.AddApiVersioning(options => { options.ApiVersionReader = new UrlSegmentApiVersionReader(); });
            services.AddVersionedApiExplorer(options =>
            {
                options.GroupNameFormat = "VVV";
                options.SubstituteApiVersionInUrl = true;
            });
            new List<string> {"1"}.ForEach(version =>
            {
                services
                    .AddOpenApiDocument(document =>
                    {
                        document.DocumentName = "v" + version;
                        document.ApiGroupNames = new[] {version};
                        document.OperationProcessors.Add(new OperationSecurityScopeProcessor("auth"));
                        document.DocumentProcessors.Add(new SecurityDefinitionAppender("auth", new OpenApiSecurityScheme
                        {
                            Type = OpenApiSecuritySchemeType.Http,
                            In = OpenApiSecurityApiKeyLocation.Header,
                            Scheme = "bearer",
                            BearerFormat = "jwt"
                        }));
                    });
            });


            services.AddIdentity<User, IdentityRole>(opt =>
                {
                    opt.User.AllowedUserNameCharacters =
                        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+ \\/";
                    opt.User.RequireUniqueEmail = true;
                    opt.Tokens.EmailConfirmationTokenProvider = EmailConfirmationTokenProviderName;
                    opt.Tokens.PasswordResetTokenProvider = ResetPasswordTokenProviderName;
                })
                .AddEntityFrameworkStores<DataMaskingContext>()
                .AddTokenProvider<RefreshTokenTokenProvider<User>>(IJwtService.RefreshTokenTokenProviderName)
                .AddDefaultTokenProviders();

            services.AddBusinessLogicLayerDI();
            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(cfg =>
                    {
                        cfg.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateAudience = true,
                            ValidateIssuer = true,
                            ValidateIssuerSigningKey = true,
                            ValidateLifetime = true,
                            ValidIssuer = "hello",
                            ValidAudience = "hello",
                            IssuerSigningKey =
                                new SymmetricSecurityKey(
                                    Encoding.UTF8.GetBytes("ghfldmsyrlfhdyrmfldw"))
                        };
                    }
                );
            services.PostConfigure<ApiBehaviorOptions>(options =>
            {
                var builtInFactory = options.InvalidModelStateResponseFactory;

                options.InvalidModelStateResponseFactory = context =>
                {
                    var logger = context.HttpContext.RequestServices
                        .GetRequiredService<ILogger<Startup>>();
                    var errorMessage = string.Join("; ", context.ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    logger.LogDebug("[Url {Url}] [Message {Message}]",
                        context.HttpContext.Request.GetDisplayUrl(), errorMessage);
                    return builtInFactory(context);
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, DataMaskingContext databaseContext,
            IServiceProvider serviceProvider)
        {
#if DEBUG
            databaseContext.Database.EnsureCreated();
#endif
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseOpenApi(a =>
            {
                a.PostProcess = (document, _) =>
                {
                    document.Schemes = new[]
                    {
                        OpenApiSchema.Https
                    };
                };
            }); // serve OpenAPI/Swagger documents
            app.UseStaticFiles();
            databaseContext.Database.Migrate();
            app.UseSwaggerUi3(); // serve Swagger UI
            app.UseApiVersioning();
            // app.UseHttpsRedirection();
            app.UseRouting();
            app.UseCors("Access-Control-Allow-Origin");
            // Redirect to Negotiate login
            app.UseStatusCodePages(async context =>
            {
                const string authPath = "/api/v1/Users/LogInByActiveDirectory";
                var response = context.HttpContext.Response;

                var requestPath = context.HttpContext.Request.Path;
                if (requestPath.HasValue && requestPath.Value != authPath &&
                    response.StatusCode == (int) HttpStatusCode.Unauthorized) response.Redirect(authPath);
            });
            app.UseAuthentication();
            app.UseAuthorization();
            //app.UseMiddleware<RequestLoggingMiddleware>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapFallbackToFile("{*path:regex(^(?=admin).*$)}", "admin/index.html");
                endpoints.MapFallbackToFile("{*path:regex(^(?!admin).*$)}", "index.html");
                endpoints.MapFallbackToFile("", "index.html");
            });
            CreateRoles(serviceProvider).Wait();
        }

        private async Task CreateRoles(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            string[] roleNames =
                {"admin", "user"};

            foreach (var roleName in roleNames)
            {
                var roleExist = await roleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                    //create the roles and seed them to the database: Question 1
                    await roleManager.CreateAsync(new IdentityRole(roleName));
            }
        }
    }
}