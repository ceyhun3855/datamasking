﻿using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace DataMasking.API.CustomTokenProviders
{
    public class RefreshTokenTokenProvider<TUser> : DataProtectorTokenProvider<TUser> where TUser : class
    {
        public RefreshTokenTokenProvider(IDataProtectionProvider dataProtectionProvider,
            IOptions<RefreshTokenTokenProviderOptions> options, ILogger<DataProtectorTokenProvider<TUser>> logger) :
            base(dataProtectionProvider, options, logger)
        {
        }
    }

    public class RefreshTokenTokenProviderOptions : DataProtectionTokenProviderOptions
    {
    }
}