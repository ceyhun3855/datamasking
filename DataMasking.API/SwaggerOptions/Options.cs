﻿namespace DataMasking.API.SwaggerOptions
{
    public class Options
    {
        public string JsonRoute { get; set; }
        public string Description { get; set; }
        public string UiEndpoint { get; set; }
    }
}