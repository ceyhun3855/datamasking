using System.ComponentModel.DataAnnotations;

namespace DeShop.API.Models.Users
{
    public class SignUpDto
    {
        [EmailAddress]
        [StringLength(70, MinimumLength = 6)]
        public string Email { get; set; } = null!;

        [StringLength(70, MinimumLength = 6)] public string Password { get; set; } = null!;
    }
}