﻿using System.ComponentModel.DataAnnotations;

namespace DeShop.API.Models.Users
{
    public class RefreshTokenDto
    {
        [StringLength(3512, MinimumLength = 1)]
        public string AccessToken { get; set; } = null!;

        [StringLength(3512, MinimumLength = 1)]
        public string RefreshToken { get; set; } = null!;
    }
}