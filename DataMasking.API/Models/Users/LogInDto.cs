using System.ComponentModel.DataAnnotations;

namespace DataMasking.API.Models.Users
{
    public class LogInDto
    {
        [EmailAddress]
        [StringLength(100, MinimumLength = 6)]
        public string Email { get; set; } = null!;

        [StringLength(100, MinimumLength = 6)] public string Password { get; set; } = null!;
    }
}