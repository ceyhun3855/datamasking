﻿using System.ComponentModel.DataAnnotations;

namespace DeShop.API.Models.Users
{
    public class ResetPasswordDto
    {
        [EmailAddress]
        [StringLength(100, MinimumLength = 6)]
        public string Email { get; set; } = null!;

        [StringLength(100, MinimumLength = 6)] public string Password { get; set; } = null!;

        [StringLength(512)] public string Token { get; set; } = null!;

        [StringLength(100, MinimumLength = 1)] public string? SessionId { get; set; }
    }
}