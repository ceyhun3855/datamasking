﻿using System.Threading.Tasks;
using DataMasking.API.Models.Users;
using DataMasking.Core.Interfaces;
using DeShop.API.Models.Users;
using DeShop.Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DataMasking.API.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IUsersService _usersService;

        public AuthController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [HttpPost("[action]")]
        [AllowAnonymous]
        public async Task<ActionResult<LoginResultModel>> LogIn([FromBody] LogInDto logInDto)
        {
            return await _usersService.LogIn(logInDto.Email, logInDto.Password);
        }

        [HttpPost("[action]")]
        [AllowAnonymous]
        public async Task<ActionResult<Result>> SignUp([FromBody] SignUpDto signUpDto)
        {
            return await _usersService.SignUp(signUpDto.Email, signUpDto.Password);
        }
    }
}